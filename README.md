# Liste de lecture

## Maquette

https://www.figma.com/file/zlessXAsmrurJoIvF96BgQ/Liste-de-lecture?node-id=0%3A1&t=8N5seChd1FWJgpwW-1

## Description
Il s'agit d'une application qui réunit les différents sites proposants des mangas, des lights novels ou autre.
Le but est de suivre facilement les sorties de nouveaux chapitres disponibles sur un large choix de site et de pouvoir activer un système de notification pour être informé en temps réel des sorties.
Pour cela, il faudra une interface simple permettant d'ajouter nos sites à partir d'une liste de sites proposés et pouvoir suggérer de nouveaux sites (nécessitera une maintenance pour vérifier le site et mettre en place la vérification de nouvelles sorties).
Chaque site ajouté informera l'utilisateur du nombre de nouvelles sorties disponibles(depuis dernière vérification ou clique sur lien de l'utilisateur) et la possibilité de voir les informations sur ces dites nouveautés.
L'utilisateur décidera des notifications (les sites inclus dans les notifications).

Il faudra donc gérer en back:
    -   les utilisateurs(comptes et mot de passe)
    -   la liste des sites suivis
    -   la totalité des sites
    -   la date de dernière connexion de l'utilisateur pour la liste des nouveautés ou la date de dernier clic sur un lien de site
    -   avoir une liste des dernieres nouveautés pour les afficher si demandé

Coté technique, il faudra pourvoir récupérer les nouveautés sur chaque site donc potentiellement une façon différente par site.

Dans un second temps, il faudra pouvoir ignorer les notifications de certaines sorties(il faudra avoir une base pour pouvoir choisir les titres à ignorer).
Il faudra également créer un tutoriel de démarrage pour ajouter un premier site.

```mermaid
erDiagram
    UTILISATEUR{
        string mail
        string password
    }
    WEBSITE{
        string nom
        string url
        LECTURE LECTURE
    }
    LECTURE{
        string titre
        file image
        chapitre CHAPITRE
    }
    CHAPITRE{
        int numero
        date sortie
    }
    NOTIFICATION{
        CHAPITRE CHAPITRE
        LECTURE LECTURE
        STRING URL
    }
    UTILISATEUR ||--o{ WEBSITE : follows
    WEBSITE ||--|{ LECTURE : has
    LECTURE ||--|{ CHAPITRE : has
    CHAPITRE o|--o| NOUVEAUTES : is
    UTILISATEUR ||--o{ NOTIFICATION : allows
    NOUVEAUTES ||--o{ NOTIFICATION : sends
    NOTIFICATION ||--o{ UTILISATEUR : send
```

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ofp-oc-cda4/sergio-gonzalez/projet-de-formation.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/ofp-oc-cda4/sergio-gonzalez/projet-de-formation/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
