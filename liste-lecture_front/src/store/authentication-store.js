import Vuex from 'vuex'

const store = new Vuex.Store({
    state: {
        authentificated : sessionStorage.getItem('authentificated') !== null
    },
    mutations : {
        change (state, payload) {
            state.authentificated = payload.authentificated
            if(payload.authentificated === 'true' || payload.authentificated === true){
                sessionStorage.setItem('authentificated', payload.token),
                sessionStorage.setItem('role', payload.role)
            }else{
                sessionStorage.removeItem('authentificated')
                sessionStorage.removeItem('role')
            }
            
        }
    },
    getters: {
        isAuthentificated: state =>  {
            if (state.authentificated === true || state.authentificated === 'true'){
                return true
            }
            return false
        },
        isAdmin: () => {
            if(sessionStorage.getItem('role') !== null
                && sessionStorage.getItem('role').includes('ROLE_ADMIN')){
                return true
            }
            return false
        }
    }
})

export default store