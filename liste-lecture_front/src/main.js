import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/authentication-store'
import { createPinia } from 'pinia'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faBars, faPlus, 
    faStar, faXmark, faArrowRightFromBracket,
     faTrashCan } from '@fortawesome/free-solid-svg-icons'
import { faStar as farStar } from '@fortawesome/free-regular-svg-icons'
import './assets/main.css'

const pinia = createPinia();

library.add(faBars, faPlus, faStar,
     farStar, faXmark, faArrowRightFromBracket, faTrashCan)

const app = createApp(App)

app.use(store)

app.use(router)

app.use(pinia)


const is_token_valid = new Promise((resolve, reject) => {
    fetch('https://api.mangaUpdate.fr/api/loggedin', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
        'token':sessionStorage.getItem('authentificated')
        })
    })
    .then(response => response.json())
    .then(data => {
        if(data.message === 'authentificated'){
            resolve(true)
            return;
        }
        let payload = {authentificated : false, token : ''}
        store.commit('change', payload)
        resolve(false)
        return
    })
})

const is_admin = new Promise((resolve, reject) => {
    fetch('https://api.mangaUpdate.fr/api/isAdmin', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
        'token':sessionStorage.getItem('authentificated')
        })
    })
    .then(response => response.json())
    .then(data => {
        if(data.message.includes('ROLE_ADMIN')){
            resolve(true)
            return;
        }
        let payload = {authentificated : true,
            token : sessionStorage.getItem('authentificated'),
            role : data.message}
        store.commit('change', payload)
        resolve(false)
        return
    })
})

router.beforeEach(async (to, from) => {
    let result = await is_token_valid;
    if((store.getters.isAuthentificated !== true || store.getters.isAuthentificated !== 'true') && !result && to.name != 'login' && to.name != 'register' && to.name != 'resetPassword' && to.name != 'resetPasswordForm' && to.name!='resetPasswordFormSent' && to.name != 'registerDone'){
      return { name: 'login' };
    }
    result = await is_admin;
    if(RegExp('/admin/').test(to.name) && !result){
        return { name: 'home' };
    }
})

app.component('font-awesome-icon', FontAwesomeIcon)

app.mount('#app')
