export default [
  {
    name: "ChapterList",
    path: "/admin/chapters/",
    component: () => import("@/views/chapter/ViewList.vue"),
  },
  {
    name: "ChapterCreate",
    path: "/admin/chapters/create",
    component: () => import("@/views/chapter/ViewCreate.vue"),
  },
  {
    name: "ChapterUpdate",
    path: "/admin/chapters/edit/:id",
    component: () => import("@/views/chapter/ViewUpdate.vue"),
  },
  {
    name: "ChapterShow",
    path: "/admin/chapters/show/:id",
    component: () => import("@/views/chapter/ViewShow.vue"),
  },
];
