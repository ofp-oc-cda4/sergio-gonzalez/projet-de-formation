import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import websiteRoutes from '@/router/website';
import bookRoutes from '@/router/book';
import chapterRoutes from '@/router/chapter';

const router = createRouter({
  history: process.env.IS_ELECTRON ? createWebHashHistory() : createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/myBooks',
      name: 'myBooks',
      component: () => import('../views/MyBooksView.vue')
    },
    {
      path: '/admin',
      name: 'admin',
      component: () => import('../views/AdminView.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('../views/RegisterView.vue')
    },
    {
      path: '/registerDone',
      name: 'registerDone',
      component: () => import('../views/RegisterDoneView.vue')
    },
    {
      path: '/resetPassword',
      name: 'resetPassword',
      component: () => import('../views/ResetPasswordView.vue'),
    },
    {
      path: '/resetPasswordReset',
      name: 'resetPasswordForm',
      component: () => import('../views/ResetPasswordFormView.vue')
      
    },
    {
      path: '/resetPasswordSent',
      name: 'resetPasswordFormSent',
      component: () => import('../views/ResetPasswordSentView.vue')
      
    },
    ...websiteRoutes,
    ...bookRoutes,
    ...chapterRoutes,
  ]
})

export default router
