export default [
  {
    name: "WebsiteList",
    path: "/admin/websites/",
    component: () => import("@/views/website/ViewList.vue"),
  },
  {
    name: "WebsiteCreate",
    path: "/admin/websites/create",
    component: () => import("@/views/website/ViewCreate.vue"),
  },
  {
    name: "WebsiteUpdate",
    path: "/admin/websites/edit/:id",
    component: () => import("@/views/website/ViewUpdate.vue"),
  },
  {
    name: "WebsiteShow",
    path: "/admin/websites/show/:id",
    component: () => import("@/views/website/ViewShow.vue"),
  },
];
