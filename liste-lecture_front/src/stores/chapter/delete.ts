import { defineStore } from "pinia";
import api from "@/utils/api";
import type { Chapter } from "@/types/chapter";
import type { DeleteState } from "@/types/stores";

interface State extends DeleteState<Chapter> {}

export const useChapterDeleteStore = defineStore("chapterDelete", {
  state: (): State => ({
    deleted: undefined,
    mercureDeleted: undefined,
    isLoading: false,
    error: undefined,
  }),

  actions: {
    async deleteItem(item: Chapter) {
      this.setError("");
      this.toggleLoading();

      if (!item?.["@id"]) {
        this.setError("No chapter found. Please reload");
        return;
      }

      try {
        await api(item["@id"], { method: "DELETE" });

        this.toggleLoading();
        this.setDeleted(item);
        this.setMercureDeleted(undefined);
      } catch (error) {
        this.toggleLoading();

        if (error instanceof Error) {
          this.setError(error.message);
        }
      }
    },

    toggleLoading() {
      this.isLoading = !this.isLoading;
    },

    setDeleted(deleted: Chapter) {
      this.deleted = deleted;
    },

    setMercureDeleted(mercureDeleted: Chapter | undefined) {
      this.mercureDeleted = mercureDeleted;
    },

    setError(error: string) {
      this.error = error;
    },
  },
});
