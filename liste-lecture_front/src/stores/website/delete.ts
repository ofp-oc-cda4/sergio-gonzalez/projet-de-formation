import { defineStore } from "pinia";
import api from "@/utils/api";
import type { Website } from "@/types/website";
import type { DeleteState } from "@/types/stores";

interface State extends DeleteState<Website> {}

export const useWebsiteDeleteStore = defineStore("websiteDelete", {
  state: (): State => ({
    deleted: undefined,
    mercureDeleted: undefined,
    isLoading: false,
    error: undefined,
  }),

  actions: {
    async deleteItem(item: Website) {
      this.setError("");
      this.toggleLoading();

      if (!item?.["@id"]) {
        this.setError("No website found. Please reload");
        return;
      }

      try {
        await api(item["@id"], { method: "DELETE" });

        this.toggleLoading();
        this.setDeleted(item);
        this.setMercureDeleted(undefined);
      } catch (error) {
        this.toggleLoading();

        if (error instanceof Error) {
          this.setError(error.message);
        }
      }
    },

    toggleLoading() {
      this.isLoading = !this.isLoading;
    },

    setDeleted(deleted: Website) {
      this.deleted = deleted;
    },

    setMercureDeleted(mercureDeleted: Website | undefined) {
      this.mercureDeleted = mercureDeleted;
    },

    setError(error: string) {
      this.error = error;
    },
  },
});
