import type { Item } from "./item";

export interface Website extends Item {
  name?: string;
  url?: string;
  books?: any;
  statut?: number;
}
