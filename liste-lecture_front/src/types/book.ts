import type { Item } from "./item";

export interface Book extends Item {
  title?: string;
  picture?: string;
  chapters?: any;
  websites?: any;
}
