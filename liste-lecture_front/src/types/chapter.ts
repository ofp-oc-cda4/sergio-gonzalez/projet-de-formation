import type { Item } from "./item";

export interface Chapter extends Item {
  number?: number;
  date?: string;
  book?: any;
}
