import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from 'vite-plugin-pwa' 

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VitePWA({
      registerType: 'autoUpdate',
      injectRegister: 'auto',
      workbox: {
        cleanupOutdatedCaches: true,
        globPatterns: [
          '**/*.{js,css,html,ico,png,svg,json\
            ,vue,txt,woff2}'
        ]
      },
      manifest: {
        name: 'MangaUpdate',
        short_name: 'mangaupdate',
        description: 'App to read manga',
        theme_color: '#181818',
        display: 'standalone',
        icons: [
          {
            src:'/icon-192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src:'/icon-256.png',
            sizes: '256x256',
            type: 'image/png'
          },
          {
            src:'/icon-388.png',
            sizes: '388x388',
            type: 'image/png'
          },
          {
            src:'/icon-512.png',
            sizes: '512x512',
            type: 'image/png'
          }
        ]
      }
    })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  base: './'
})
