/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./src/App.vue",
    "./src/**/*.vue",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}